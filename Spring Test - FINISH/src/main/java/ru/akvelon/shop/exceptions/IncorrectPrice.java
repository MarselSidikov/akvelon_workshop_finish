package ru.akvelon.shop.exceptions;

public class IncorrectPrice extends RuntimeException {
    public IncorrectPrice(String message) {
        super(message);
    }
}
