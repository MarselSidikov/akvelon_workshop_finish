package ru.akvelon.shop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.akvelon.shop.dto.DiscountDto;
import ru.akvelon.shop.dto.OrdersPricesDto;
import ru.akvelon.shop.dto.DiscountsForPricesDto;
import ru.akvelon.shop.exceptions.IncorrectPrice;
import ru.akvelon.shop.exceptions.IncorrectType;
import ru.akvelon.shop.models.Discount;
import ru.akvelon.shop.repositories.DiscountsRepositoryDataJpa;
import ru.akvelon.shop.repositories.DiscountsRepositoryJdbc;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DiscountsServiceImpl implements DiscountsService {

    private final DiscountsRepositoryDataJpa discountsRepository;

    private final DiscountsRepositoryJdbc discountsRepositoryJdbc;

    @Override
    public List<DiscountDto> getDiscountsByType(String type) {
        Discount.Type discountType;
        try {
            discountType = Discount.Type.valueOf(type);
        } catch (IllegalArgumentException e) {
            throw new IncorrectType();
        }
        return DiscountDto.from(discountsRepository.findAllByType(discountType));
    }

    @Override
    public DiscountsForPricesDto applyDiscounts(OrdersPricesDto ordersPrices) {
        if (ordersPrices.getPrices().stream().anyMatch(value -> value < 0)) {
            throw new IncorrectPrice("Price must be positive");
        }
        return discountsRepositoryJdbc.applyAllDiscounts(ordersPrices.getPrices());
    }
}
